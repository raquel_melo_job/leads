package com.br.itau.Leads.controllers;

import com.br.itau.Leads.enums.TiposDeLead;
import com.br.itau.Leads.models.Lead;
import com.br.itau.Leads.models.Produto;
import com.br.itau.Leads.services.LeadService;
import com.br.itau.Leads.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(LeadController.class)
public class LeadControllerTest {

    @MockBean
    LeadService leadService;

    @MockBean
    ProdutoService produtoService;

    @Autowired
    private MockMvc mockMvc;

    //converte objeto java para json
    ObjectMapper objectMapper = new ObjectMapper();

    Lead lead;
    Produto produto;

    @BeforeEach
    public void inicializar() {
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Raqueeel");
        lead.setEmail("email@email.com");
        lead.setTipoDeLead(TiposDeLead.QUENTE);

        produto = new Produto();
        produto.setNome("Café");
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setDescricao("Santa Clara");

        lead.setProdutos(Arrays.asList(produto));
    }

    @Test
    public void testarCadastroDeLead() throws Exception {

        Iterable<Produto> produtoIterable = Arrays.asList(produto);

        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(produtoService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);

        String json = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/lead")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated()) //verifica se o status de resposta é 201 CREATED
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));
                // teste de erro .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(2)));

    }

    @Test
    public void testarBuscarTodosLeads() throws Exception {

        Iterable<Lead> leadIterable = Arrays.asList(lead);
        Mockito.when(leadService.buscarTodosLeads()).thenReturn(leadIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/lead")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Raqueeel")));
                // teste de erro .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Vini")));

    }

    @Test
    public void testarBuscarLead() throws Exception {

        Optional<Lead> leadOptional = Optional.of(lead);

        Mockito.when(leadService.buscarPorId(Mockito.any(Integer.class))).thenReturn(leadOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/lead/" + lead.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()) //verifica se o status de resposta é 200 OK
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(lead.getId())));
                // teste de erro .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(3)));
    }

    @Test
    public void testarAtualizarLead() throws Exception {

        lead.setEmail("novoemail@gmail.com");

        Mockito.when(leadService.atualizarLead(Mockito.any(Lead.class))).thenReturn(lead);

        String json = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/lead/"+lead.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk()) //verifica se o status de resposta é 200 OK
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.equalTo(lead.getEmail())));
                // teste de erro  .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.equalTo("email@email.com")));
    }

    @Test
    public void testarDeletarLead() throws Exception {
        leadService.deletarLead(lead);

        String json = objectMapper.writeValueAsString(lead);

        Mockito.verify(leadService, Mockito.times(1)).deletarLead(Mockito.any(Lead.class));

        mockMvc.perform(MockMvcRequestBuilders.delete("/lead/"+lead.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent()); //verifica se o status de resposta é 200 OK
        // teste de erro  .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.equalTo("email@email.com")));

        //Mockito.verify(leadService.deletarLead(Mockito.any(Lead.class)));
    }
}
