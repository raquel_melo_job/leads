package com.br.itau.Leads.controllers;

import com.br.itau.Leads.models.Produto;
import com.br.itau.Leads.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTest {

    @MockBean
    ProdutoService produtoService;

    @Autowired
    private MockMvc mockMvc;

    //converte objeto java para json
    ObjectMapper objectMapper = new ObjectMapper();

    Produto produto;

    @BeforeEach
    public void inicializar() {
        produto = new Produto();
        produto.setNome("Café");
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setDescricao("Santa Clara");

    }

    @Test
    public void testarCadastroDeProduto() throws Exception {

        Iterable<Produto> produtoIterable = Arrays.asList(produto);

        Mockito.when(produtoService.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated()) //verifica se o status de resposta é 201 CREATED
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
        // teste de erro .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(2)));

    }

    @Test
    public void testarBuscarTodosProdutos() throws Exception {

        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(produtoService.buscarTodosProdutos()).thenReturn(produtoIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/produto")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Café")));
        // teste de erro .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Vini")));

    }

    @Test
    public void testarBuscarProduto() throws Exception {

        Optional<Produto> produtoOptional = Optional.of(produto);

        Mockito.when(produtoService.buscarPorId(Mockito.any(Integer.class))).thenReturn(produtoOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/produto/" + produto.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()) //verifica se o status de resposta é 200 OK
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(produto.getId())));
        // teste de erro .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(3)));
    }

    @Test
    public void testarAtualizarProduto() throws Exception {

        produto.setPreco(9.50);

        Mockito.when(produtoService.atualizarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.put("/produto/"+produto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk()) //verifica se o status de resposta é 200 OK
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco", CoreMatchers.equalTo(9.50)));
        // teste de erro  .andExpect(MockMvcResultMatchers.jsonPath("$.preco", CoreMatchers.equalTo(12.00)));
    }

    @Test
    public void testarDeletarProduto() throws Exception {
        produtoService.deletarProduto(produto);

        String json = objectMapper.writeValueAsString(produto);

        Mockito.verify(produtoService, Mockito.times(1)).deletarProduto(Mockito.any(Produto.class));

        mockMvc.perform(MockMvcRequestBuilders.delete("/produto/"+produto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNoContent()); //verifica se o status de resposta é 200 OK
        // teste de erro  .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.equalTo("email@email.com")));

        //Mockito.verify(produtoService.deletarProduto(Mockito.any(Produto.class)));
    }
}
