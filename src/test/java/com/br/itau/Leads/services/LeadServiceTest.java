package com.br.itau.Leads.services;

import com.br.itau.Leads.enums.TiposDeLead;
import com.br.itau.Leads.models.Lead;
import com.br.itau.Leads.models.Produto;
import com.br.itau.Leads.repositories.LeadRepository;
import com.br.itau.Leads.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    LeadRepository leadRepository;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    LeadService leadService;

    Lead lead;

    Produto produto;

    @BeforeEach
    public void inicializar(){
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Raqueeel");
        lead.setEmail("email@email.com");
        lead.setTipoDeLead(TiposDeLead.QUENTE);

        produto = new Produto();
        produto.setNome("Café");
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setDescricao("Santa Clara");

        lead.setProdutos(Arrays.asList(produto));
    }

    @Test
    public void testarSalvarLead(){

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadService.salvarLead(lead);

        Assertions.assertEquals(lead, leadObjeto);
    }

    @Test
    public void testarAtualizarLead(){

        lead.setEmail("novoemailservico@gmail.com");

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadService.salvarLead(lead);

        Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());
    }

    @Test
    public void testarBuscarPorId(){

        Optional<Lead> optionalLead = Optional.of(lead);

        Mockito.when(leadRepository.findById(Mockito.any(Integer.class))).thenReturn(optionalLead);

        Assertions.assertEquals(lead, optionalLead.get());
    }

    @Test
    public void testarBuscaTodos(){

        Iterable<Lead> leadIterable = Arrays.asList(lead);

        Mockito.when(leadRepository.findAll()).thenReturn(leadIterable);

        Assertions.assertEquals(lead, ((List<Lead>) leadIterable).get(0));
    }


    @Test
    public void testarDeletarLead(){
        leadService.deletarLead(lead);
        Mockito.verify(leadRepository).delete(Mockito.any(Lead.class));
    }
}
