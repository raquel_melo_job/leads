package com.br.itau.Leads.services;

import com.br.itau.Leads.models.Lead;
import com.br.itau.Leads.models.Produto;
import com.br.itau.Leads.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Optional<Produto> buscarPorId(int id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        return optionalProduto;
    }

    public Produto salvarProduto(Produto produto){
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Produto atualizarProduto(Produto produto){
        Optional<Produto> produtoOptional = buscarPorId(produto.getId());
        if(produtoOptional.isPresent()){
            Produto produtoData = produtoOptional.get();

            if(produto.getNome() == null){
                produto.setNome(produtoData.getNome());
            }
            if(produto.getDescricao() == null){
                produto.setDescricao(produtoData.getDescricao());
            }
            if(produto.getPreco() == null){
                produto.setPreco(produtoData.getPreco());
            }
        }
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public void deletarProduto(Produto produto){
        produtoRepository.delete(produto);
    }

    public Iterable<Produto> buscarTodosProdutos(List<Integer> produtosId){
        Iterable<Produto> produtos = produtoRepository.findAllById(produtosId);
        return produtos;
    }

    public Iterable<Produto> buscarTodosProdutos() {
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }
}
