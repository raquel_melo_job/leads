package com.br.itau.Leads.services;

import com.br.itau.Leads.models.Lead;
import com.br.itau.Leads.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LeadService {

    //repository nunca é usado em controller, o service que envia pro controller
    @Autowired
    private LeadRepository leadRepository;

    public Optional<Lead> buscarPorId(int id){
        Optional<Lead> optionalLead = leadRepository.findById(id);
        return optionalLead;
    }

    public Lead salvarLead(Lead lead){
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Lead atualizarLead(Lead lead){
        Optional<Lead> leadOptional = buscarPorId(lead.getId());
        if(leadOptional.isPresent()){
            Lead leadData = leadOptional.get();

            if(lead.getNome() == null){
                lead.setNome(leadData.getNome());
            }
            if(lead.getEmail() == null){
                lead.setEmail(leadData.getEmail());
            }
            if(lead.getTipoDeLead() == null){
                lead.setTipoDeLead(leadData.getTipoDeLead());
            }
        }
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public void deletarLead(Lead lead){
        leadRepository.delete(lead);
    }

    public Iterable<Lead> buscarTodosLeads(){
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;
    }
}
