package com.br.itau.Leads.enums;

public enum TiposDeLead {
    QUENTE,
    ORGANICO,
    FRIO,
}
