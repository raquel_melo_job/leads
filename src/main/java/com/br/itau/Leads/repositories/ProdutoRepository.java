package com.br.itau.Leads.repositories;

import com.br.itau.Leads.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
}
