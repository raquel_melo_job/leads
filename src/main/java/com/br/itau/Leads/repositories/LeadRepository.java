package com.br.itau.Leads.repositories;

import com.br.itau.Leads.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {
}
