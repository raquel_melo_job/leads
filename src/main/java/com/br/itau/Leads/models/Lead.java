package com.br.itau.Leads.models;

import com.br.itau.Leads.enums.TiposDeLead;
import org.hibernate.procedure.ProcedureOutputs;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nome_completo")
    @Size(min=8, max = 100, message = "O nome deve ter entre 8 e 100 caracteres")
    private String nome;

    @Email(message = "O formato do e-mail é invalido")
    private String email;
    private TiposDeLead tipoDeLead;

    @ManyToMany
    private List<Produto> produtos;

    public Lead() {
    }

    public Lead(String nome, String email, TiposDeLead tipoDeLead) {
        this.nome = nome;
        this.email = email;
        this.tipoDeLead = tipoDeLead;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TiposDeLead getTipoDeLead() {
        return tipoDeLead;
    }

    public void setTipoDeLead(TiposDeLead tipoDeLead) {
        this.tipoDeLead = tipoDeLead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
