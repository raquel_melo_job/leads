package com.br.itau.Leads.controllers;

import com.br.itau.Leads.models.Produto;
import com.br.itau.Leads.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public Iterable<Produto> buscarTodosProdutos(){
        return produtoService.buscarTodosProdutos();
    }

    @GetMapping("/{id}")
    public Produto buscarProduto(@PathVariable int id){
        Optional<Produto> produtoOptional = produtoService.buscarPorId(id);
        if (produtoOptional.isPresent()){
            return produtoOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping()
    public ResponseEntity<Produto> adicionarProduto(@RequestBody Produto produto){
        Produto novoProduto = produtoService.salvarProduto(produto);
        return ResponseEntity.status(201).body(novoProduto);
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable Integer id, @RequestBody Produto produto){
        produto.setId(id);
        Produto produtoObjeto = produtoService.atualizarProduto(produto);
        return produtoObjeto;
    }

    @DeleteMapping("/{id}")
    public Produto deletarProduto(@PathVariable int id){
        Optional<Produto> produtoOptional = produtoService.buscarPorId(id);
        if (produtoOptional.isPresent()){
            produtoService.deletarProduto(produtoOptional.get());
            return produtoOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);

    }
}
