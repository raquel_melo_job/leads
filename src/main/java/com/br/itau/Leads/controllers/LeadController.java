package com.br.itau.Leads.controllers;

import com.br.itau.Leads.models.Lead;
import com.br.itau.Leads.models.Produto;
import com.br.itau.Leads.services.LeadService;
import com.br.itau.Leads.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/lead")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public Iterable<Lead> buscarTodosLeads(){
        return leadService.buscarTodosLeads();
    }

    @GetMapping("/{id}")
    public Lead buscarLead(@PathVariable int id){
        Optional<Lead> leadOptional = leadService.buscarPorId(id);
        if (leadOptional.isPresent()){
            return leadOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping()
    public ResponseEntity<Lead> adicionarLead(@RequestBody @Valid Lead lead){
        List<Integer> produtosId = new ArrayList<>();
        for(Produto produto:lead.getProdutos()){
            produtosId.add(produto.getId());
        }
        Iterable<Produto> produtos = produtoService.buscarTodosProdutos(produtosId);

        lead.setProdutos((List) produtos);

        Lead novoLead = leadService.salvarLead(lead);
        return ResponseEntity.status(201).body(novoLead);
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable Integer id, @RequestBody Lead lead){
        lead.setId(id);
        Lead leadObjeto = leadService.atualizarLead(lead);
        return leadObjeto;
    }

    @DeleteMapping("/{id}")
    public Lead deletarLead(@PathVariable int id){
        Optional<Lead> leadOptional = leadService.buscarPorId(id);
        if (leadOptional.isPresent()){
            leadService.deletarLead(leadOptional.get());
            return leadOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);

    }
}
